const { body } = require('express-validator');

exports.feedbackValidation = [
   body('stars')
        .not()
        .isEmpty()
        .withMessage('condition is required')
        .isNumeric()
        .exists({ checkNull: true }),
        
    body('content')
        .not()
        .isEmpty()
        .withMessage('content is required'),
];