const { body } = require('express-validator');

exports.proposalValidation = [
   body('condition')
        .not()
        .isEmpty()
        .withMessage('condition is required'),
        
    body('home_pickup')
        .not()
        .isEmpty()
        .withMessage('home_pickup is required'),
       
    body('home_delivery')
        .not()
        .isEmpty()
        .withMessage('home_delivery is required'),
        
    body('details_content')
        .not()
        .isEmpty()
        .withMessage('details_content is required'),
    
    body('quantity_left')
        .not()
        .isEmpty()
        .withMessage('quantity_left is required'),
        
    body('accepted_payment')
        .not()
        .isEmpty()
        .withMessage('firstname is required')
        .isNumeric(),

];