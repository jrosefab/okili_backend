const { check } = require('express-validator');

exports.signupValidation = [
    check('email')
        .not()
        .isEmpty()
        .isEmail()
        .withMessage("must be an email"),
        
    check('password')
        .not()
        .isEmpty()
        .withMessage("must be filled"),

        //.matches(/^(?.*[a-z]{3})[a-z0-9]+$/i)
       // .withMessage('Password should not be empty, minimum eight characters, at least one letter, one number and one special character'),
    
    check('lastname')
        .not()
        .isEmpty()
        .withMessage('lastname is required'),

    check('firstname')
        .not()
        .isEmpty()
        .withMessage('firstname is required'),
]

exports.updateAuthValidation = [
    check('email')
        .not()
        .isEmpty()
        .isEmail()
        .withMessage("must be an email"),
        
    check('password')
        .not()
        .isEmpty()
        .withMessage("must be filled"),
]

