const { body } = require('express-validator');
const UserSchema = require("../models/user.schema");

exports.userDataValidation = [
   body('lastname')
        .not()
        .isEmpty()
        .withMessage('lastname is required'),
        
    body('firstname')
        .not()
        .isEmpty()
        .withMessage('firstname is required'),
];