const dotenv = require("dotenv");
const env = dotenv.config();

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

if(!env){
    throw new Error("The .env file is missing. Please create one with env variables");
}

const config = {
    PORT : process.env.port || 8080,
    MONGO_URI : process.env.MONGO_URI,
    JWT_SECRET : process.env.JWT_SECRET,
    SECRET_SESSION : process.env.SECRET_SESSION
}

module.exports = config;