const mongoose = require("mongoose");
const config = require("./index");

const options = { useNewUrlParser : true, 
                  useUnifiedTopology: true,
                  useCreateIndex: true,
                  useFindAndModify : false
                };

mongoose.connect(config.MONGO_URI, options);

const conn = mongoose.connection;

conn.on("error", () => {
    console.log("✖✖✖ error occured from the database ✖✖✖");
});

conn.once("open", () => {
    console.log("◊◊◊ database successfully connected ◊◊◊");
});

module.exports = mongoose;