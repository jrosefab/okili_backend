const express = require('express');
const { singupController, 
        loginController,
        updateAuthController, 
        logoutController,
       } = require("../controllers/auth.controller");
const { uploadProfileController } = require("../controllers/file.controller");

const { signupValidation, 
        updateAuthValidation 
       } = require("../validations/auth.validation");

const { hasAuthorisationController } = require("../controllers/checking.controller");
const { getUserByIdController }      = require("../controllers/user.controller");
const ensureLoggedIn                 = require('connect-ensure-login').ensureLoggedIn();

const router = express.Router();

// GET routes
router.get('/auth/logout', logoutController);

// POST routes
router.post('/auth/signup',/* signupValidation,*/ uploadProfileController, singupController);
router.post('/auth/login', loginController);

// PUT routes
router.put('/auth/:userId/update', 
            ensureLoggedIn,
            hasAuthorisationController,
            updateAuthValidation,
            updateAuthController);

// PARAMS
router.param('userId', getUserByIdController);

module.exports = router;