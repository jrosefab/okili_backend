const { createAdController,
        updateAdController, 
        getAllAdsController,
        getAdByIdController,
        getSingleAdController,
        deleteAdController,
        getMyAdListController
       } = require("../controllers/ads.controller");
const { getUserByIdController }   = require("../controllers/user.controller");
const express                     = require('express');
const { onlyAdCreatorController } = require("../controllers/checking.controller");
const { uploadPublicationController } = require("../controllers/file.controller");
const ensureLoggedIn              = require('connect-ensure-login').ensureLoggedIn();
const router                      = express.Router();


router.get('/ads', getAllAdsController);
router.get('/ad/:adId', getSingleAdController);
router.get('/ads/:userId', ensureLoggedIn, getMyAdListController);

// POST routes
router.post('/ad/create', 
            ensureLoggedIn, 
            uploadPublicationController, 
            createAdController); //if not logged, back to login

// PUT routes
router.put('/ad/:adId/update', 
            onlyAdCreatorController,
            uploadPublicationController,
            updateAdController)

// DELETE routes
router.delete('/ad/:adId/delete', 
              onlyAdCreatorController, 
              deleteAdController);

// PARAMS
router.param('adId', getAdByIdController);
router.param('userId', getUserByIdController);

module.exports = router;