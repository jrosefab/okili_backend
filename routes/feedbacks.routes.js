const express = require('express');

const { getFeedbackController,
        getAllFeedbacksController, 
        postFeedbackController,
        updateFeedbackController,
        deleteFeedbackController,
        getFeedbackByIdController 
       } = require("../controllers/feedbacks.controller");

const {getUserByIdController} = require("../controllers/user.controller");

const { everyOneExcpetMeController, 
       onlyFeedbackCreatorController 
      } = require('../controllers/checking.controller');

const { feedbackValidation } = require('../validations/feedback.validation');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const router = express.Router();

// GET routes
router.get('/feedbacks/:userId', getAllFeedbacksController);
router.get('/feedbacks/:feedbackId', getFeedbackController);

// POST routes
router.post('/feedbacks/:userId/create', 
            ensureLoggedIn, 
            everyOneExcpetMeController,
            postFeedbackController);

// PUT routes
router.put('/feedbacks/:feedbackId/update', 
            ensureLoggedIn, 
            onlyFeedbackCreatorController,
            feedbackValidation,
            updateFeedbackController);

// DELETE routes
router.delete('/feedbacks/:feedbackId/delete', 
               ensureLoggedIn, 
               onlyFeedbackCreatorController,
               deleteFeedbackController);
 
// PARAMS
router.param('userId', getUserByIdController);
router.param('feedbackId', getFeedbackByIdController);

module.exports = router;
