const express = require('express');
const multer = require('multer');
const { upload } = require('../services/multer.service')
const path = require("path");

const { getUserDataController,
        updateUserDataController,
        updateUserPictureController,
        deleteUserController,
        getUserByIdController 
       } = require("../controllers/user.controller");

const { userDataValidation } = require('../validations/user.validation');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const router = express.Router();

// GET routes
router.get('/user/:userId/', getUserDataController);

// PUT routes
router.put('/user/:userId/update', 
            ensureLoggedIn, 
            userDataValidation,
            updateUserDataController);

router.put('/user/:userId/update/picture', 
            ensureLoggedIn, 
            updateUserPictureController);

// DELETE routes
router.delete('/user/:userId/delete', 
              ensureLoggedIn, 
              deleteUserController);
 
// PARAMS
router.param('userId', getUserByIdController);

module.exports = router;
