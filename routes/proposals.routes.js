const express = require('express');
const { getProposalsFromAdController,
        createProposalController, 
        updateProposalController,
        getProposalByIdController,
        getMyProposalController,
        deleteProposalController,
        acceptProposalController,
        declineProposalController,
        closeProposalController,
       } = require("../controllers/proposals.controller");

const { onlyPropCreatorController,
        whoCanDecideController,
        whoCanProposeController 
       } = require("../controllers/checking.controller");

const { getAdByIdController }      = require("../controllers/ads.controller")
const { uploadProposalController }  = require("../controllers/file.controller")
const { proposalValidation }       = require("../validations/proposal.validation")
const ensureLoggedIn               = require('connect-ensure-login').ensureLoggedIn();

const router = express.Router();


// GET home page.
router.get('/:adId/proposals', getProposalsFromAdController); // return propositions from attached to ad
router.get('/prop/:propId', 
             whoCanDecideController,
             getMyProposalController); // return one proposition and all infos

// POST routes
router.post('/:adId/new/proposal/', 
            ensureLoggedIn, 
            whoCanProposeController,
           // proposalValidation,
            uploadProposalController,
            createProposalController); // submit new proposition
            
// PUT routes
router.put('/prop/:propId/update', 
            ensureLoggedIn, 
            onlyPropCreatorController, 
            proposalValidation,
            updateProposalController) // update a proposition I did

router.put('/prop/:propId/accept', 
            ensureLoggedIn, 
            whoCanDecideController, 
            acceptProposalController) // only ad creator have right to accept

router.put('/prop/:propId/decline', 
            ensureLoggedIn, 
            whoCanDecideController, 
            declineProposalController)  // only ad creator have right to decline

router.put('/prop/:propId/close', 
            ensureLoggedIn, 
            whoCanDecideController, 
            closeProposalController) // only ad creator have right to close

// DELETE routes
router.delete('/prop/:propId/delete', 
              onlyPropCreatorController,
              deleteProposalController); // delete proposal from my repository

// PARAMS
router.param('adId', getAdByIdController);
router.param('propId', getProposalByIdController);

module.exports = router;