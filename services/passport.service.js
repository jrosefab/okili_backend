const passport = require("passport");
const bcrypt = require('bcrypt');
const UserSchema = require("../models/user.schema");
const LocalStrategy = require('passport-local').Strategy;

passport.serializeUser( (user, done) => {
    return done(null, user.id);
});

passport.deserializeUser( async(id , done) => {
    try {
        const userObj = await UserSchema.findById(id, 'password');
        return done(null, userObj);
    }catch(error){
        done(error);
    };
});

passport.use(new LocalStrategy({ 
        passReqToCallback : true,
        usernameField : "email",
        passwordField : "password" 
    }, async (req, email, password, done) => {
        const userObj = await UserSchema.findOne({ email : email }); 
        if(userObj && userObj._id){
            const match = await bcrypt.compare(password, userObj.password);
            if (match){
                return done(null, {
                    id : userObj._id,
                });
            };
        };
        return done(null, false);
    }
));

module.exports = passport;