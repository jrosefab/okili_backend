const multer = require('multer');
const path = require("path");
const conn = require('../config/database');

// Handle Profile
const storageProfile = multer.diskStorage({
  destination: 'public/uploads/profile',
 filename: (req, file, cb) => {
    cb(null,file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  },
  fileFilter: (req, file, cb) => {
    checkFileType(file, cb);
  },
});

exports.uploadProfile = multer({
  storage: storageProfile,
  limits:{fileSize: 1000000}
}).single('picture_user');


// Handle Publication
const storagePublication = multer.diskStorage({
  destination: './public/uploads/publications',
  filename: (req, file, cb) => {
    cb(null,file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  },
  fileFilter: (req, file, cb) => {
    checkFileType(file, cb);
  }
});

exports.uploadPublication = multer({
  storage: storagePublication,
  limits:{fileSize: 1000000}
}).single('picture_publication');

// Handle Proposals
const storageProposal = multer.diskStorage({
  destination: './public/uploads/proposals',
  filename: (req, file, cb) => {
    cb(null,file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  },
  fileFilter: (req, file, cb) => {
    checkFileType(file, cb);
  }
});

exports.uploadProposal = multer({
  storage: storageProposal,
  limits:{fileSize: 1000000}
}).single('picture_proposal');


// CHECKING FILE METHODE 
const checkFileType =(file, cb) => {
  const filetypes = /jpeg|jpg|png|gif/;
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  const mimetype = filetypes.test(file.mimetype);

  if(mimetype && extname) return cb(null,true);
  else cb('Error: Images Only!');
};