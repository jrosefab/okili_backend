const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const { Schema, ObjectId } = mongoose;

const UserSchema = new Schema({
    profileId : {
        type : ObjectId
    },
    role : { 
        type : String,
        enum: ['Customer', 'Admin'],
        default : "Customer",
    },
    firstname : { 
        type : String,
        required : true
    },
    lastname: {
        type: String,
        required : true
    },
    email : {
        type : String,
        required : true
    },
    password : {
        type : String,
    },
    phone_number : {
        numero : { 
            type : Number,
            required : true,
        },
        isVerified : {
            type : Boolean,
            default : false
        }
    },
    address : {
        type : String,
        required : true
    },
    picture : {
        size : String,
        name : String,
        source : String
    },
    signaleddBy : {
        type : String,
    },
    localisation : {
        type : String,
    },
    feedbacks : [{
        id : {
            type : ObjectId,
            ref : "Feedback"
            }
    }],
    stars : {
        type : Number,
        default : 0
    },
    reactivity : {
        type : String,
    },
    updated_date : { 
        type : Date,
        default : Date.now
    },
    created_date : { 
        type : Date,
        default : Date.now
    },
    salt: String,
});

UserSchema.pre("save" , function(next){
    var profileId = new mongoose.Types.ObjectId;
    if (this.profileId == undefined){
        this.profileId = profileId
    };
    next();
})

UserSchema.methods.hashPassword = function(password){
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
}

UserSchema.methods.comparePassword = function(password, hash){
    return bcrypt.compareSync(password, hash);
}

module.exports = mongoose.model('User', UserSchema);