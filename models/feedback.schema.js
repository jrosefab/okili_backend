const mongoose = require("mongoose");
const { Schema, ObjectId } = mongoose;

const FeedbackSchema = new Schema({
    created_by : {            
        type : ObjectId,
        ref : "User"
    },
    stars : { type : Number },
    content : { type : String },
    user_reference : {
        type : ObjectId,
        ref : "User"
    },
    created_date : { 
        type : Date,
        default : Date.now
    },
    updated_date : { 
        type : Date,
        default : Date.now
    }
});

module.exports = mongoose.model('Feedback', FeedbackSchema);