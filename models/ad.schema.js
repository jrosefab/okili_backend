const mongoose = require("mongoose");
const { Schema, ObjectId } = mongoose;

const AdSchema = new Schema({
    kind : { 
        type : String, //shopping or research
        required : true
    },
    category : {
        type : String, // consumable or equipment
        required : true
    },
    title: {
        type: String,
        required: true,
    },
    tags : [{ type : String }],
    pictures : [{
        size : String,
        name : String,
        source : String
    }],
    urgency : {
        type : String,
        required : true
    },
    delivery_mode : {
        home_delivery : {
            type : Boolean,
            required : true,
            default : true
        },
        home_pickup : {
            type : Boolean,
            required : true,
            default : false
        },
        home_pickup_details : {
            type : String,
            default : ""
        },
        delivery_km_acceptation : {
            type : Number,
            default : 0
        },
    },
    geolocation : {
        address : {
            type : String,
            required : true,
            default : ""
        },
        coordinates : {
            latitude : {
                type : Number,
                required : true
            },
            longitude : {
                type : Number,
                required : true
            }, 
        }
    },
    details_content : {
        type : String
    },
    current_condition : {
        condition : {
            type : String, // bad // new // neutral
        },
        quantity_left : {
            type : String,
            default : "0",
        }
    },
    geolocation : {
        address : {
            type : String, // bad // new // neutral
        },
        latitude : {
            type : Number,
            default : 0,
        },
        longitude : {
            type : Number,
            default : 0,
        }
    },
    created_date : { 
        type : Date,
        default : Date.now
    },
    updated_date : { 
        type : Date,
    },
    created_by : {
        type : ObjectId,
        ref : "User",
        //required : true
    },
    proposals : [{
        id : {
            type : ObjectId,
            ref : "Proposal"
        },
        state :  {
            type : String,
            default : "WAITING"
        }
    }],
    price : { 
        type : Number,
        required : true
    }
});

module.exports = mongoose.model('Ad', AdSchema);