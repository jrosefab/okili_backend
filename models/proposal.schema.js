const mongoose = require("mongoose");
const  { AdSchema } = require("./ad.schema");

const { Schema, ObjectId } = mongoose

const ProposalSchema = new Schema({
    state : {
        type : String,
        default : "WAITING" // DECLINED // ACCEPTED // CLOTURED
    },
    purchase_steps : {
        isPaid : {
            type : Boolean
        },
        qrCode : {
            type : String,
            default : null // PROGRESS // PAID //
        },
        secretPass : {
            type : String,
        }
    },
    current_condition : {
        condition : {
            type : String, // bad // new // neutral
        },
        quantity_left : {
            type : String,
            default : "0",
        }
    },
    geolocation : {
        address : {
            type : String, // bad // new // neutral
        },
        latitude : {
            type : Number,
            default : 0,
        },
        longitude : {
            type : Number,
            default : 0,
        }
    },
    delivery_mode : {
        home_delivery : {
            type : Boolean,
            required : true,
            default : true
        },
        home_pickup : {
            type : Boolean,
            required : true,
            default : false
        },
        home_pickup_details : {
            type : String,
            default : ""
        },
        delivery_km_acceptation : {
            type : Number,
            default : 0
        },
    },
    picture : {
        size : String,
        name : String,
        source : String
    },
    details_content : {
        type : String,
    },
    ad_reference : {
        type : ObjectId,
        ref : "Ad"
    },
    created_date : { 
        type : Date,
        default : Date.now
    },
    updated_date : { 
        type : Date,
        default : Date.now
    },
    created_by : {
        type : ObjectId,
        ref : "User",
        //required : true
    },
    accepted_payment : { 
        type : Number,
        required : true
    }
});


module.exports = mongoose.model('Proposal', ProposalSchema);