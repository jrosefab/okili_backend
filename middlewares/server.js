const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const session = require("express-session");
const config = require("../config");
const passport = require("../services/passport.service");
const authRouter = require('../routes/auth.routes');
const adsRouter = require('../routes/ads.routes');
const proposalsRouter = require('../routes/proposals.routes');
const userRouter = require('../routes/user.routes');
const feedbacksRouter = require('../routes/feedbacks.routes');
const flash = require('connect-flash');
const methodOverride = require('method-override');

const app = express();

app.use(cors());
app.use(flash());

app.use(morgan("dev"));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
    cookieName : 'session',
    secret: config.SECRET_SESSION,
    resave : true,
    saveUninitialized: true,
}));
app.use(methodOverride('_method'));


// passport
app.use(passport.initialize());
app.use(passport.session());

// asset files
app.use(express.static('./public/'));

app.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET,HEAD,OPTIONS,POST,PUT,DELETE"
    );
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin,Cache-Control,Accept,X-Access-Token ,X-Requested-With, Content-Type, Access-Control-Request-Method"
    );
    if (req.method === "OPTIONS") {
        return res.status(200).end();
    }
    next();
});

// routes
app.use('/', authRouter);
app.use('/', adsRouter);
app.use('/', proposalsRouter);
app.use('/', userRouter);
app.use('/', feedbacksRouter);

module.exports = app;