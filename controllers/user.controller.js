const UserSchema           = require("../models/user.schema");
const AdSchema             = require("../models/ad.schema");
const { validationResult } = require('express-validator');
const { uploadProfile }    = require('../services/multer.service')

// GET controller
exports.getUserDataController = (req, res, next) => {
    req.userData.password = undefined;
    UserSchema.findById(req.userData)
              .populate('feedbacks.from', '_id lastname firstname')
              .exec((err, userData) => {
                if(err || !userData){
                    return res.status(400).json({ error : err })
                };
                return res.status(200).send(userData);
              });
};


// POST controller
exports.acceptProposalController = (req, res, next) => {
    const ad = req.prop.ad_reference._id;
    ProposalSchema.find({ ad_reference : ad, state : "ACCEPTED" })
                  .exec(( err, prop ) => {
                        if(err) return res.status(400).json({ error : err });
                        if(prop.length !== 0 ){
                            return res.json({ message : "You've already accept a proposition", prop });
                        }else{
                            ProposalSchema.findByIdAndUpdate( req.prop ,
                                                            { state : "ACCEPTED"},
                                                            { new : true },
                                                            (err, result) => {
                                                                if (err) 
                                                                    return res.status(500).json({error : err });
                                                                else 
                                                                    return res.status(200).json({ message : "congratulation, propsition accepted successfully " });
                                                            });
                        };
                    });
};

// PUT controller
exports.updateUserPictureController = (req, res, next) => {
    const id = req.user._id;
    uploadProfile(req, res, (err) => {
        if(err){
          return res.status(400).json({ error : err })
        } else {
            if(req.file == undefined){
                res.status(400).json({ error : "No file selected" })
            }else{
                console.log(req.file)
                const newProfilePicture = {
                    contentType: req.file.mimetype,
                    size: req.file.size,
                    name: req.file.originalname,
                    path: req.file.path
                };

                UserSchema.findOneAndUpdate({ _id: id }, 
                                            { picture : newProfilePicture}, 
                                            { new : true })
                        .then(result => { 
                                return res.status(200).json({ result : result }); 
                        })
                        .catch(err => { return res.status(500).json({ error : err }); });
          };
        };
    });
};

exports.updateUserDataController = (req, res, next) => {
    const errors = validationResult(req);
    const id = req.user._id;
    const newUserData = { 
        firstname : req.body.firstname,
        lastname : req.body.lastname,
    }

    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    };

    UserSchema.findOneAndUpdate({_id : id }, newUserData)
              .exec()
              .then(result => { 
                return res.status(200).json({ result : result }); 
              })
              .catch(err => { return res.status(500).json({ error : err }); });
};


// DELETE controller
exports.deleteUserController = (req, res) => {
    const prop = req.prop;
    const ad = req.prop.ad_reference._id;

    AdSchema.findByIdAndUpdate( ad,
                               { $pull : { proposals : { _id : prop._id } }},
                               { new : true })
            .exec(( err, result ) => {
                if(err){
                    console.log("Error occured while deleting proposal from ad");
                }else{ 
                    prop.remove((err, prop) =>{
                        if(err) return res.status(400).json({ error : err });
                        res.json({ message: "Proposal deleted successfully" });
                    });
                };
            });
    
};

// PARAMETER controller
exports.getUserByIdController = (req, res, next, id) => {
    UserSchema.findOne({ profileId : id })
              .exec((err, userData) => {
                if(err || !userData){
                    return res.status(400).json({
                        error : "This user does not exist"
                    });
                };
                req.userData = userData
                next();
                });
};