const { uploadPublication, uploadProfile, uploadProposal } = require('../services/multer.service');

exports.uploadPublicationController = (req, res, next) => {
    var path = '/uploads/publications/'
    uploadPublication(req, res, (err) =>{
        if(err){
            return res.status(400).json({ error : err });
        }else{
            if(req.file == undefined){
                res.status(400).json({ error : "No file selected" });
            }else{
                
                const publicationPictures = [{
                    size        : req.file.size,
                    name        : req.file.originalname,
                    source      : path + req.file.filename,
                }];
                req.publicationPictures = publicationPictures;
                next();
            };
        };
    });
};

exports.uploadProfileController = (req, res, next) => {
    var path = '/uploads/profile/'
    uploadProfile(req, res, (err) =>{
        if(err){
            return res.status(400).json({ error : err });
        }else{
            if(req.file == undefined){
                var profilePicture = {
                    size        : 0.02,
                    name        : "default_picture",
                    source      : path + "default_picture.png",
                }
                req.profilePicture = profilePicture;
                next();
            }else{
                var profilePicture = {
                    size        : req.file.size,
                    name        : req.file.originalname,
                    source      : path + req.file.filename,
                };
                req.profilePicture = profilePicture;
                next();
            };
        };
    });
};

exports.uploadProposalController = (req, res, next) => {
    var path = '/uploads/proposals/'
    uploadProposal(req, res, (err) =>{
        if(err){
            return res.status(400).json({ error : err });
        }else{
            if(req.file == undefined){
                var proposalPictures = {
                    size        : 0.02,
                    name        : "default_picture",
                    source      : path + "proposal_box.png",
                }
                req.proposalPictures = proposalPictures;
                next();
            }else{
                var proposalPictures = {
                    size        : req.file.size,
                    name        : req.file.originalname,
                    source      : path + req.file.filename,
                };
                req.proposalPictures = proposalPictures;
                next();
            };
        };
    });
};