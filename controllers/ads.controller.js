const AdSchema              = require("../models/ad.schema");
const { validationResult }  = require('express-validator');

// GET controller
exports.getAllAdsController = (req, res, next) => {
    AdSchema.find()
            .sort({created_date : -1})
            .exec(( err, ads) => {
                if(err || !ads){
                    return res.status(400).json({
                        error : err
                    });
                };
                req.ads = ads;
                res.json(ads);
                next();
            });
};

exports.getSingleAdController = (req, res) => {
    AdSchema.findById(req.ad.id)
            .populate("created_by", "profileId")
            .exec(( err, ad) => {
                if(err || !ad){
                    return res.status(400).json({ error : err })
                };
                return res.status(200).send(ad);
            });
};

exports.getMyAdListController = (req, res, next) => {
    const id = req.user._id
    AdSchema.find({ created_by : id })
            .exec(( err, adList) => {
                if(err || !adList){
                    return res.status(400).json({
                        error : err
                    });
                };
                req.adList = adList;
                res.json(adList);
                next();
            });
};


// POST controller
exports.createAdController = (req, res, next) => {
    const errors = validationResult(req);
        if (!errors.isEmpty()){
            return res.status(422).json({ errors: errors.array() });
    };

    const ad = new AdSchema({
        kind          : req.body.kind,
        category      : req.body.category,
        title         : req.body.title,
        delivery_mode : {
            home_pickup             : req.body.home_pickup,
            home_delivery           : req.body.home_delivery,
            home_pickup_details     : req.body.home_pickup_details,
            delivery_km_acceptation : req.body.delivery_km_acceptation 
        },
        urgency     : req.body.urgency,
        tags        : req.body.tags,
        geolocation : {
            address   : req.body.address,
            latitude  : req.body.latitude,
            longitude : req.body.longitude
        },
        details_content : req.body.details_content,
        current_condition : {
            quantity_left : req.body.quantity_left,
            condition     : req.body.condition
        },
        price      : req.body.price,
        created_by : req.user._id,
        pictures   : req.publicationPictures
    });
    ad.save((err, ad) => {
        if(err) return res.status(403).send({error : err});
        else return res.send(ad);
    });
};

// PUT controller
exports.updateAdController = (req, res, next) => {
    const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
    };

    const ad = { 
        kind            : req.body.kind,
        category        : req.body.category,
        title           : req.body.title,
        tags            : req.body.tags,
        urgency         : req.body.urgency,
        delivery_mode   : { home_pickup :  req.body.home_pickup,
                            home_delivery :  req.body.home_delivery },
        details_content : req.body.details_content,
        quantity_left   : req.body.quantity_left,
        localisation    : req.body.localisation,
        price           : req.body.price,
        updated_date    : Date.now(),
        pictures        : req.publicationPictures
    };

    AdSchema.findByIdAndUpdate(req.ad._id, ad, {new : true })
            .then( result => {
                return res.status(200).send(result);
            })
            .catch(err => {
                return res.status(500).send(err);
            });
};

// DELETE controller
exports.deleteAdController = (req, res) => {
    const ad = req.ad;
    ad.remove((err, ad) => {
        if(err) return res.status(400).json({ error: err });
        res.json({ message: "Le post a été supprimé !" });
    });
};

// PARAMETER controller
exports.getAdByIdController = (req, res, next, id) => {
    AdSchema.findById(id)
            .populate('created_by', '_id firstname lastname')
            .populate('proposition', '_id created_date')
            .exec((err, ad) => {
                if(err || !ad){
                    return res.status(400).json({
                        error : err
                    });
                };
                req.ad = ad;
                next();
            });
};

// Checking Method
