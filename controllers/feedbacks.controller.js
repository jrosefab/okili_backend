const FeedbackSchema = require("../models/feedback.schema");
const { validationResult } = require('express-validator');
const UserSchema = require("../models/user.schema");

// GET controller
exports.getFeedbackController = (req, res, next) => {
    FeedbackSchema.findById(req.feedback)
                   .populate('created_by', '_id lastname firstname')
                   .exec((err, fb) => {
                       console.log(fb)
                        if(err || !fb){
                            return res.status(400).json({ error : err })
                        };
                        return res.status(200).send(fb);
                    });
};

exports.getAllFeedbacksController = (req, res, next) => {
    FeedbackSchema.find({ user_reference : req.userData.profileId })
                   .populate('created_by', '_id lastname firstname')
                   .exec((err, fbs) => {
                        if(err || !fbs){
                            return res.status(400).json({ error : err })
                        };
                        return res.status(200).send(fbs);
                    });
};

// POST controller
exports.postFeedbackController = (req, res, next) => {
    const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
    };
    const feedback = new FeedbackSchema();
    feedback.stars           = req.body.stars;
    feedback.content         = req.body.content;
    feedback.created_date    = Date.now();
    feedback.created_by      = req.user._id;
    feedback.user_reference  = req.userData.profileId;

    FeedbackSchema.find({ user_reference : req.userData.profileId,
                          created_by     : req.user._id })
                  .exec(( err, fb ) => {
                    if (err) return res.status(400).json({ error : err });
                    if (fb.length !== 0 ){
                        return res.json({ message : "You can post only one feedback per user "})
                    }else{
                        UserSchema.findOneAndUpdate({ profileId : req.userData.profileId }, 
                                                    { $push : { feedbacks : {_id : feedback._id } }}, 
                                                    { new : true })
                                  .then(
                                    feedback.save((err, feedback) => {
                                        if(err) res.status(403).send(err);
                                        else res.send(feedback);
                                  }))
                                  .catch(err => {
                                        return res.status(500).send(err);
                                  });
                    };
                  });
};

// PUT controller
exports.updateFeedbackController = (req, res, next) => {
    const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
    };
    const newFeedback = { 
        stars        :  req.body.stars,
        content      :  req.body.content,
        updated_date : Date.now()
    }

    FeedbackSchema.findByIdAndUpdate( req.feedback._id, 
                             newFeedback, 
                            { new : true })
                  .then(result => {
                      console.log(result)
                    return res.status(200).send("feedback updated successfuly")
                  })
                  .catch(err => {
                    return res.status(500).send(err)
                  });
};

// DELETE controller
exports.deleteFeedbackController = (req, res) => {
    const feedback = req.feedback;
    const userId = req.feedback.user_reference;

    UserSchema.update( userId,
                       { $pull : { feedbacks : { _id : feedback._id } }},
                       { safe: true })
                  .exec(( err, result ) => {
                        if(err){
                            console.log("Error occured while deleting proposal from ad");
                        }else{ 
                            feedback.remove((err, prop) =>{
                                if(err) return res.status(400).json({ error : err });
                                res.json({ message: "Feedback deleted successfully" });
                        });
                    };
            });
};

// PARAMETER controller
exports.getFeedbackByIdController = (req, res, next, id) => {
    FeedbackSchema.findById(id)
                  .populate('created_by', '_id')
                  .populate('user_reference', '_id')
                  .exec((err, fb) => {
                    if(err || !fb){
                        return res.status(400).json({
                            error : "this element does not exist"
                        });
                    };
                    req.feedback = fb;
                    next();
                });
};