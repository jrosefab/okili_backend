const UserSchema           = require("../models/user.schema");
const passport             = require("../services/passport.service");
const { validationResult } = require('express-validator');
const bcrypt               = require("bcrypt");

// GET controller
exports.logoutController = (req, res) => {
  req.logout();
  return res.status(200).send("User is not logged anymore");
}

// POST controller

exports.singupController = (req, res) => {
  const errors = validationResult(req);
    if (!errors.isEmpty()){
        return res.status(422).json({ errors: errors.array() });
  };

  UserSchema.findOne({ email : req.body.email }, (err, doc) => {
    if(err) res.status(500).send("error occured");
    else {
      if(doc) res.status(500).send("Email already exists");
      else {
        var user = new UserSchema({
            email        : req.body.email.toLowerCase(),
            password     : req.body.password,
            firstname    : req.body.firstname,
            lastname     : req.body.lastname,
            phone_number : {
              numero : req.body.phone_number
            },
            address      : req.body.address,
            picture      : req.profilePicture
        });
        console.log(user)
        user.password = user.hashPassword(req.body.password);
        
        user.save((err, user)=>{
          if(err) {res.status(403).send(err); console.log(err)}
          else res.send(user);
        });
      };
    };
  });
};

exports.loginController = (req, res, next) => {
  passport.authenticate('local', (err, user) => {
    if (err) return next(err);
    if (!user){ 
      return res.status(401).send({ message : "Wrong email or password" });
    }
    req.logIn(user, (err) => {
      if (err) return next(err); 

      UserSchema.findById(user.id)
                .exec((err, user) => {
                  if(err || !user){
                      return res.status(400).send({
                          error : err
                      });
                  };
                  return res.status(200).send(user.profileId);
              });
    });
  })(req, res, next);
};

// PUT controller
exports.updateAuthController = (req, res, next) => {
  const id = req.userData.profileId;
  const newEmail = req.body.email;
  const newPassword = req.body.password;

  const errors = validationResult(req);
  if (!errors.isEmpty()) 
      return res.status(422).json({ errors: errors.array() });

  const hash = password => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
  }

  UserSchema.findOne({ profileId : id })
            .then((user)=>{
                Object.assign(user, { email : newEmail, password : hash(newPassword) });
                user.save();
            })
            .then((newUser) =>{
                return res.status(200).json({ message : 'user is updated with success' });
            })
           .catch((err) => {
              return res.status(500).json(err);
           });   
};





