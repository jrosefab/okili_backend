const { validationResult } = require('express-validator');
const ProposalSchema = require("../models/proposal.schema");
const AdSchema = require("../models/ad.schema");

// GET controller
exports.getMyProposalController = (req, res) => {
    return res.json(req.prop);
};

exports.getProposalsFromAdController = (req, res) => {
    ProposalSchema.find()
                  .select("title delivery_mode price created_date")
                  .exec(( err, prop) => {
                      if(err || !prop){
                          return res.status(400).json({
                              error : err
                          });
                      };
                      req.prop = prop
                      res.json(prop)
                      next();
                  });
};

exports.getAllProposalsController = (req, res, next) => {
    ProposalSchema.find()
                  .select("title delivery_mode price created_date")
                  .exec(( err, prop) => {
                        if(err || !prop){
                            return res.status(400).json({
                                error : err
                            });
                        };
                        req.prop = prop
                        res.json(prop)
                        next();
                   });
};

// POST controller
exports.createProposalController = (req, res, next) => {
    const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
    };

    const prop = new ProposalSchema({
        current_condition : {
            quantity_left : req.body.quantity_left,
            condition     : req.body.condition
        },
        delivery_mode : {
            home_pickup             : req.body.home_pickup,
            home_delivery           : req.body.home_delivery,
            home_pickup_details     : req.body.home_pickup_details,
            delivery_km_acceptation : req.body.delivery_km_acceptation 
        },
        details_content : req.body.details_content,
        geolocation : {
            address   : req.body.address,
            latitude  : req.body.latitude,
            longitude : req.body.longitude
        },
        accepted_payment : req.body.accepted_payment,
        ad_reference : req.ad._id,
        created_by   : req.user._id,
        pictures   : req.proposalPictures
    });
    
    AdSchema.findByIdAndUpdate(req.ad._id, 
                              {$push : {proposals : {_id : prop._id, state : "WAITING"}}},
                              { new : true })
            .then(  
                prop.save((err, prop) => {
                    if(err) return res.status(500).send(err);
                    else return res.status(200).send(prop);
            }))
            .catch(err => {
                return res.status(500).send(err)
            });
}

// PUT controller
exports.updateProposalController = (req, res, next) => {
    const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
    };
    const newProp = { 
    condition                  : req.body.condition,
    delivery_mode              : { home_pickup :  req.body.home_pickup,
                                   home_pickup :  req.body.home_delivery },
    delivery_modehome_delivery :  req.body.home_delivery,
    details_content            :  req.body.details_content,
    localisation               :  req.body.localisation,
    quantity_left              :  req.body.quantity_left,
    accepted_payment           :  req.body.accepted_payment,
    created_by                 :  req.user._id,
    updated_date               :  Date.now() }
    
    ProposalSchema.findByIdAndUpdate(req.prop._id, newProp, {new : true })
                  .then( result => {
                    return res.status(200).send(result)
                  })
                  .catch(err => {
                    return res.status(500).send(err)
                  });
};

exports.acceptProposalController = (req, res, next) => {
    const ad = req.prop.ad_reference._id;
    ProposalSchema.find({ ad_reference : ad, state : "ACCEPTED" })
                  .exec(( err, prop ) => {
                        if(err) return res.status(400).json({ error : err });
                        if(prop.length !== 0 ){
                            return res.json({ message : "You've already accept a proposition", prop });
                        }else{
                            ProposalSchema.findByIdAndUpdate( req.prop ,
                                                            { state : "ACCEPTED"},
                                                            { new : true },
                                                            (err, result) => {
                                                                if (err) 
                                                                    return res.status(500).json({error : err });
                                                                else 
                                                                    return res.status(200).json({ message : "congratulation, propsition accepted successfully " });
                                                            });
                        };
                    });
};

exports.declineProposalController = (req, res, next) => { 
    ProposalSchema.findByIdAndUpdate( req.prop ,
                                     { state : "DECLINED"},
                                     { new : true },
                                     (err, result) => {
                                        if (err) 
                                            return res.status(500).json({error : err });
                                        else 
                                            return res.status(200).json(
                                            { message : "congratulation, propsition declined successfully" }
                                        );
                                    });
};

exports.closeProposalController = (req, res, next) => {
    const ad = req.prop.ad_reference._id;
    ProposalSchema.find({ ad_reference : ad, purchase_steps : { isPaid : true } })
                  .exec(( err, prop ) => {
                        if(err) return res.status(400).json({ error : err });
                        if(prop.length !== 0 ){
                            return res.json({ message : "You've already accept a proposition", prop });
                        }else{
                            ProposalSchema.findByIdAndUpdate( req.prop ,
                                                            { state : "ACCEPTED"},
                                                            { new : true },
                                                            (err, result) => {
                                if (err) return res.status(500).json({error : err });
                                else return res.status(200).json({ message : "congratulation, propsition accepted successfully " });
                                                            });
                        };
                    });
};

// DELETE controller
exports.deleteProposalController = (req, res) => {
    const prop = req.prop;
    const adId = req.prop.ad_reference._id;

    AdSchema.findByIdAndUpdate( adId,
                               { $pull : { proposals : { _id : prop._id } }},
                               { safe: true })
            .exec(( err, result ) => {
                if(err){
                    console.log("Error occured while deleting proposal from ad");
                }else{ 
                    prop.remove((err, prop) =>{
                        if(err) return res.status(400).json({ error : err });
                        res.json({ message: "Proposal deleted successfully" });
                    });
                };
            });
    
};

// PARAMETER controller
exports.getProposalByIdController = (req, res, next, id) => {
    ProposalSchema.findById(id)
                  .populate('created_by', '_id')
                  .populate('ad_reference', '_id created_date')
                  .exec((err, prop) => {
                    if(err || !prop){
                        return res.status(400).json({
                            error : "this element does not exist"
                        });
                    };
                    req.prop = prop;
                    next();
                });
};