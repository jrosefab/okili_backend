const UserSchema            = require("../models/user.schema");
const AdSchema              = require("../models/ad.schema");
const FeedbackSchema             = require("../models/feedback.schema");

// CHECKING method
exports.hasAuthorisationController = (req, res, next) => {
    if(req.userData !== undefined){
      UserSchema.findById(req.user._id)
                .then(user => {
                  if(!user.profileId.equals(req.userData.profileId)){
                      return res.status(403).json({ error : "Action not allowed"});
                  };
                  next();
                })
                .catch(err => {
                  return res.status(500).json(err);
                });
    };
};

exports.onlyAdCreatorController = (req, res, next) => {
    const id = req.ad._id;
    AdSchema.findById(id)
            .then((ad) =>{
                if(!ad.created_by.equals(req.user._id)){
                    return res.status(403).json({ error : "Action not allowed"});
                };
                next();
            })
            .catch(err => {
                return res.status(500).json(err);
            });
};

exports.onlyPropCreatorController = (req, res, next) => {
    if(req.prop !== undefined){
      UserSchema.findById(req.user._id)
                .then(user => {
                    if(!user._id.equals(req.prop.created_by._id)){
                        return res.status(403).json({ error : "Action not allowed"});
                    };
                    next();
                })
                .catch(err => {
                    return res.status(500).json(err)
                });
    };
};

exports.onlyFeedbackCreatorController = (req, res, next) => {
    if(req.feedback !== undefined){
      FeedbackSchema.findById(req.feedback._id)
                    .then(fb => {
                        if(!fb.created_by._id.equals(req.user._id)){
                            return res.status(403).json({ error : "Action not allowed"});
                        };
                        next();
                    })
                    .catch(err => {
                        return res.status(500).json(err)
                    });
    };
};

exports.whoCanDecideController = (req, res, next) => {
    const id = req.prop.ad_reference._id;
    AdSchema.findById(id)
            .then((ad) =>{
                if(!ad.created_by.equals(req.user._id)){
                    return res.status(403).json({ error : "Action not allowed"});
                }
                next();
            })
            .catch(err => {
                return res.status(500).json(err);
            });
};

exports.whoCanProposeController = (req, res, next) => {
    AdSchema.findById(req.ad._id)
            .then((ad) => {
                if(ad.created_by.equals(req.user._id)){
                  return res.status(403).json({ error : "You can't propose on your own ad" });
                };
                next();
            })
            .catch(err => {
                return res.status(500).json(err);
            });
};

exports.everyOneExcpetMeController = (req, res, next) => {
    UserSchema.findById(req.user._id)
            .then((user) => {
                if(user._id.equals(req.userData.profileId)){
                  return res.status(403).json({ error : "You can't leave yourself feedbacks" });
                };
                next();
            })
            .catch(err => {
                return res.status(500).json(err);
            });
};
