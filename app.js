import config from "./config";
import server from "./middlewares/server";

server.listen(config.PORT, () => {
    console.log(`◎◎◎ application running on port ${config.PORT} ◎◎◎`);
});